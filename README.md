# Тестовый шаблон докера

Перед запуском меняем при необходимоста имя рабочей дериктории которое по умолчанию `/template`, так же меняем названия
контейнеров которые так же имеют префикс `template`.

Замена  нужна в файлах `docker-compose.yml`, `Makefile`, `/docker/nginx/default.conf`, `/docker/ngnix/Dockerfile`,
`/docker/php-fpm/Dockerfile`

## Запуск

`make build`

Добавляем права при необходимоста на папку `storage`

Проверяем в браузере [localhost:8078](http://localhost:8078/)



## Конфиг
* **php** = 8.0-fpm при необходимости меняем на нужный в `/docker/php-fpm/Dockerfile`
* **nginx** = 1.17
* **postgres** = 12
* **pgadmin** = dpage/pgadmin4
* **redis** = 6.2
* **rabbitmq** = 3-management
* **elasticsearch** = 7.5.2



## Возможные проблемы

Если есть проблемы с доступам к папкам,
`/storage/logs` и `/storage/frameworks` и `/bootstrap/cache`,
обычно бывает при работе с фреймворками. Заходим на пк через терминал в рабочую
дерикторию и делаем следующее.


Заходим в корень проекта пишем:

`sudo chown -R $USER:$USER .`

`sudo chmod -R 775 .`

`sudo chmod -R 775 storage`

`sudo chmod -R ugo+rw storage`

далее:

`sudo chown -R www-data:www-data ./storage/logs/`

`sudo chown -R www-data:www-data ./storage/framework/`

`sudo chown -R www-data:www-data ./bootstrap/cache`

`sudo chmod -R 775 ./bootstrap/cache`

Если все равно есть проблемы делаем так

`sudo chown -R $USER:www-data storage`
`sudo chown -R $USER:www-data bootstrap/cache`

Затем

`chmod -R 775 storage`
`chmod -R 775 bootstrap/cache`


## pgadmin

Заходим в админку, по умолчанию [localhost:5050](http://localhost:5050/)
Логин: `admin@admin.com`
Пароль: `root`

Создаем подключение с БД которую создали через docker-compose, работаем.

Значение `host` указываем `postgres` или так, как прописано название сервиса у вас в docker-compose


### Не открывается pgadmin? 

Скорее всего проблема с правами? заходим в папку `/docker` на компьютере через терминал и вводим команду

`sudo chown -R 5050:5050 pgadmin/`
